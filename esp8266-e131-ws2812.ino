// Version: 0.2
// Date: 03-16-2023
// Author: Andreas Schulz
// License: GNU General Public License
#include <NeoPixelBus.h>
#include <ESPAsyncE131.h>
// PIN 3 must be used for ESP8266 
#define PIN 3 
// Total number of LEDs 
#define LEDTOTAL 528
// Each universe can manage a maximum of 170 LEDs. Therefore, the LEDs are divided into different universes 
// Leds per universe ->  528 / 170 = 3.1 So we use 4 universes with 132 LEDs
#define LEDPERUNIVERSE 132
// Used universes 
#define UNIVERSES 4
// First Channel. 1 Chanel per Color and LED starting with 1 
//  Chanel 1 is first color on first LED
#define FIRST_CHANEL 1

// Setup WLAN 
const char* ssid = "***";
const char* pass = "***";

// Setup static IP for your network 
IPAddress ip(192,168,0,196);
IPAddress gateway(192,168,0,1);
IPAddress subnet(255,255,255,0);

ESPAsyncE131 e131(FIRST_CHANEL);

NeoPixelBus <NeoGrbFeature, Neo800KbpsMethod> strip(LEDTOTAL,PIN);

// I initialize the variables for the loops at this point so that the system does not have to reinitialize every time it is run 
// This saves computing power 
// The array neocolor is used to bring the universes together. Three color codes must be recorded per LED

uint8_t neocolor[LEDTOTAL][3]={{0}};
int j=0;
int pos=0;
int i=0;
// Counter for cycles without Wi-Fi connection
int wlancounter=0;

void setup() 
{
 // Starting Output on the Serial Console
 Serial.begin(115200);
 // WLAN initialisieren
 WiFi.config(ip,gateway,subnet);
 WiFi.begin(ssid,pass);
 // wait for WiFi to be ready
 while (WiFi.status() != WL_CONNECTED)
 {
  delay(500);
 }
 // Stay connected to Wi-Fi and automatically reconnect if the connection is lost

 WiFi.setAutoReconnect(true);
 WiFi.persistent(true);

 // starting E1.31
 // Output occurs only if Serial.begin(115200) is not commented on
 if(e131.begin(E131_UNICAST))
 {
  Serial.println(F("Ready for receiving ... "));
 }
 else
 {
  Serial.println(F("E1.31 fails ... "));
 }
  // starting LEDs
  strip.Begin();
  strip.Show();
}

void loop() 
{
  //Restarts the NodeMCU if the WLAN is unavailable for 150 cycles
  if(WiFi.status() != WL_CONNECTED)
  {
    wlancounter++;
  }
  else
  {
    wlancounter=0;
  }
  if(wlancounter>=150)
  {
    ESP.restart();
  }
  // check if an E131 packet has been received.
  if(!e131.isEmpty())
  {
    // Packet abholen
    e131_packet_t packet;
    e131.pull(&packet);
    // Check whether the arrived package is intended for the predefined universes.
    if (htons(packet.universe) > 0 && htons(packet.universe) <= UNIVERSES)
    {
      // Retrieve all values and assign them to the appropriate LEDs
      for(i=0; i < LEDPERUNIVERSE; i++)
      {
        j = i*3+FIRST_CHANEL;
        // Calculate the position in the LED strip
        pos=(htons(packet.universe)-1)*LEDPERUNIVERSE+i;
        // get red
        neocolor[pos][0]=packet.property_values[j];
        // get green
        neocolor[pos][1]=packet.property_values[j+1];
        // get blue
        neocolor[pos][2]=packet.property_values[j+2];
      }
      // When the last universe has been reached, the LED strip can be updated.
      if(htons(packet.universe) == UNIVERSES) draw();
    }
  }
}
void draw()
{
  for(i=0; i < LEDTOTAL; i++)
  {
    // create color object
    RgbColor color(neocolor[i][0], neocolor[i][1], neocolor[i][2]);
    // LED position and color transferred to the NeoPixelBus.
    strip.SetPixelColor(i,color); 
  }
  // If all values have been transferred, update LEDs
  strip.Show();
}
