# ESP8266 with E1.31 and WS2812 LED

This project allows the control of WS2812 LEDs with an ESP2866 NodeMCU with the E1.31 protocol. So you can synchronize your lighting with your PC and create great lighting effects.

## Features
* Supports up to 1530 LED per NodeMCU
* 170 LED per Universe
* max 63999 Universes

## Configuration
- Requires :
- [Arduino IDE](https://www.arduino.cc/en/software)
- ESP8266 Librarys from [https://arduino.esp8266.com/stable/package_esp8266com_index.json](https://arduino.esp8266.com/stable/package_esp8266com_index.json)
- ESPAsyncE1.31 from [https://github.com/forkineye/ESPAsyncE131](https://github.com/forkineye/ESPAsyncE131) (in Arduino IDE included)
- ESPAsyncUDP from [https://github.com/me-no-dev/ESPAsyncUDP](https://github.com/me-no-dev/ESPAsyncUDP) (NOT in Arduino IDE included)
- NeoPixelBus from [https://github.com/Makuna/NeoPixelBus/wiki](https://github.com/Makuna/NeoPixelBus/wiki) (in Arduino IDE included)

## Warning
- This program has not yet been tested on ESP32 models.
- Do not use the Adafruit Neopixel or FastLED. These lead to sporadic restarts on ESP8266 and ESP32 models.
- The control of many units causes high data traffic in the WLAN. For large installations, use a separate Wi-Fi network.

## Usage

1. Open the script with the Arduino IDE and select your ESP8266 NoceMCU. 
2. Replace the required values in the script.
3. Compile the scetch and load it onto your ESP8266 node.

## Tips
To control and synchonize the LED strips with your PC, I recommend using OpenRGB. OpenRGB supports numerous hardware, Linux, Windows, Mac and the E1.31 protocol.
Various plugins allow numerous lighting effects that inspire.

[https://gitlab.com/CalcProgrammer1/OpenRGB#](https://gitlab.com/CalcProgrammer1/OpenRGB#)

A German-language video tutorial will be made available on YouTube. [https://www.youtube.com/watch?v=i6s0z4zZ_4I](https://www.youtube.com/watch?v=i6s0z4zZ_4I)
